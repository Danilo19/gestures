//
//  PinchViewController.swift
//  Gestures
//
//  Created by Danilo Angamarca on 17/1/18.
//  Copyright © 2018 Danilo Angamarca. All rights reserved.
//

import UIKit

class PinchViewController: UIViewController {

    @IBOutlet weak var imagenView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imagenView.isUserInteractionEnabled = true
        let pich = UIPinchGestureRecognizer(target: self, action: #selector(self.pinch))
        imagenView.addGestureRecognizer(pich)
    }

    @objc func pinch(sender: UIPinchGestureRecognizer) {
        sender.view?.transform = (sender.view?.transform.scaledBy(x: sender.scale, y: sender.scale))!
        sender.scale = 1.0
        
    }
    

}
