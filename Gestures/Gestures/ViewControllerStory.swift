//
//  ViewControllerStory.swift
//  Gestures
//
//  Created by Danilo Angamarca on 17/1/18.
//  Copyright © 2018 Danilo Angamarca. All rights reserved.
//

import UIKit

class ViewControllerStory: UIViewController {

    @IBOutlet weak var customView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func SwipeLeft(_ sender: Any) {
        customView.backgroundColor = .yellow
    }
    
    
    @IBAction func SwipeRight(_ sender: Any) {
        customView.backgroundColor = .red
    }
    
    
    @IBAction func SwipeDown(_ sender: Any) {
        customView.backgroundColor = .green
    }
    
    
    @IBAction func SwipeUp(_ sender: Any) {
        let action = sender as! UISwipeGestureRecognizer
        customView.backgroundColor = .orange
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
