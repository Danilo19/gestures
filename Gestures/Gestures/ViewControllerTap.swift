//
//  ViewControllerTap.swift
//  Gestures
//
//  Created by Danilo Angamarca on 17/1/18.
//  Copyright © 2018 Danilo Angamarca. All rights reserved.
//

import UIKit

class ViewControllerTap: UIViewController {

    @IBOutlet weak var LabelTouche: UILabel!
    
    @IBOutlet weak var tapLabel: UILabel!
    
    @IBOutlet weak var coordenadasLabel: UILabel!
    
    @IBOutlet weak var CustomView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touchCount = touches.count
        let tap = touches.first
        let tapCount = tap?.tapCount
        
        LabelTouche.text = "\(touchCount ?? 0)"
        tapLabel.text = "\(tapCount ?? 0)"
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        let point = touch?.location(in: self.view)
        
        let x = point?.x
        let y = point?.y
        
        print("X: \(x!), y: \(y!)")
        self.coordenadasLabel.text = "\(x!), \(y!)"
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Termino")
    }
    
    
    @IBAction func tapAction(_ sender: UITapGestureRecognizer) {
     
       CustomView.backgroundColor = .red
        
    }
    
}
